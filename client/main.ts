﻿/// <reference path="../types-gt-mp/index.d.ts" />
/// <reference path="enums/Include.ts" />
/// <reference path="classes/Include.ts" />
/// <reference path="objects/Include.ts" />
/// <reference path="race/Include.ts" />

API.onResourceStart.connect(()=>{
    API.sendNotification("Starting...")
    //Player.invincible = true
    TrackBuilder.init()
})

API.onResourceStop.connect(()=>{
    Entity.disposeAll()
    Race.dispose();
})

API.onKeyDown.connect((sender, e)=>{
    switch (e.KeyCode) {
        case Keys.M:
        TrackBuilder.toggleMenu()
        break;
    }
})

API.onServerEventTrigger.connect((event, args) => {
    switch(event){
        case "race":
        Race.onEvent(args)
        break

        case "toggleAirbreak":
        Airbreak.toggle()
        break

        case "toggleBuilder":
        TrackBuilder.toggle()
        break

        case "TrackbBuilder":
        TrackBuilder.onEvent(args)
        break

        case "clean":
        Entity.disposeAll()
        break
        
        case "reload":

        break
    }
});

API.onUpdate.connect(() => {
    Player.update()
    Airbreak.update()
    TrackBuilder.update()
    Race.onUpdate()
});
