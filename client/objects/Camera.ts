class Camera {
    /// Static
    private static activeCam = null
    static get active(){
        return this.activeCam
    }

    /// Members
    handle: GrandTheftMultiplayer.Client.Javascript.GlobalCamera
    
    constructor(pos: Vector3, rot: Vector3){
        if(pos == null) {
            this.handle = API.getActiveCamera()
            return;
        }
        this.handle = API.createCamera(pos, rot)
    }
    
    destroy(){
        API.deleteCamera(this.handle)
    }

    setActive(duration = 0, easepos = true, easerot = true){
        if(duration != 0){
            API.interpolateCameras(
                API.getActiveCamera(),
                this.handle,
                duration,
                easepos,
                easerot
            )
        } else {
            API.setActiveCamera(this.handle)
        }
        Camera.activeCam = this;
    }

    shake(type: string, amp:number){
        API.setCameraShake(this.handle, type, amp);
    }

    get position(){
        return API.getCameraPosition(this.handle)
    }
    set position(pos){
        API.setCameraPosition(this.handle, pos)
    }

    get rotation(){
        return API.getCameraRotation(this.handle)
    }
    set rotation(rot){
        API.setCameraRotation(this.handle, rot)
    }
}