// Marker härstammar och utökar från Entity 
// (inherits and extends)
class Marker extends Entity {
    // Constructor
    // tar in valfri färg (rgba) parameter
    constructor(
        type: MarkerType,
        pos,
        rot,
        scale,
        rgba = new RGBA(255, 255, 255, 255)
    ){
        super(API.createMarker(
            type,
            pos,
            new Vector3(),
            rot,
            scale,
            rgba.r, rgba.g, rgba.b, rgba.a
        ));
    }

    /* Accessors */
    // Getter, var dir = marker.Direction
    get direction(){
        return API.getMarkerDirection(this.handle)
    }
    // Setter, marker.direction = new Vector3(x, y, z)
    set direction(dir){
        API.setMarkerDirection(this.handle, dir)
    }

    get color(){
        let c = API.getMarkerColor(this.handle)
        return new RGBA(c.R, c.G, c.B, c.A)
    }
    set color(rgba: RGBA){
        API.setMarkerColor(this.handle, rgba.a, rgba.r, rgba.g, rgba.b)
    }

    get bobbing(){
        return API.getMarkerBobUpAndDown(this.handle)
    }
    set bobbing(toggle){
        API.setMarkerBobUpAndDown(this.handle, toggle)
    }
}