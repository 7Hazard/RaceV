class Cursor {
    /* Statics */

    /* Accessors */
    static get enabled(){
        return API.isCursorShown()
    }
    static set enabled(toggle){
        API.showCursor(toggle)
    }

    static get position(){
        return API.getCursorPosition()
    }

    static get worldPos(){
        return API.screenToWorldMaintainRatio(this.position)
    }

    /* Functions */
}