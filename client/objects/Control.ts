class Control {


    static isPressed(control: GameControl){
        return API.isControlPressed(control)
    }

    static justPressed(control: GameControl){
        return API.isControlJustPressed(control)
    }

    static disable(control: GameControl){
        API.disableControlThisFrame(control)
    }
}