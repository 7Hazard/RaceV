class Entity {
    /* Statics */
    private static entities: Entity[] = []

    static disposeAll() {
        this.entities.forEach(element => {
            element.dispose()
        });

        API.sendNotification("Disposed all entities")
    }

    /* Members */
    public handle: LocalHandle

    protected constructor(handle: LocalHandle) {
        this.handle = handle
        Entity.entities.push(this)
    }

    dispose() {
        API.deleteEntity(this.handle)
    }

    get position() {
        return API.getEntityPosition(this.handle)
    }
    set position(pos) {
        API.setEntityPosition(this.handle, pos)
    }

    getGround() {
        return API.getGroundHeight(this.position)
    }
    ground() {
        let pos = this.position
        this.position = new Vector3(pos.X, pos.Y, this.getGround())
    }

    get rotation() {
        return API.getEntityRotation(this.handle)
    }
    set rotation(rot) {
        API.setEntityRotation(this.handle, rot)
    }

    get velocity() {
        return API.getEntityVelocity(this.handle)
    }
    set velocity(vel) {
        API.setEntityVelocity(this.handle, vel)
    }
}