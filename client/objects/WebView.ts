class WebView {
    private browser: GrandTheftMultiplayer.Client.GUI.CEF.Browser

    constructor(url, local, x, y, height, width, visible = true, wait = true){
        this.browser = API.createCefBrowser(width, height, local)
        if(wait){
            API.waitUntilCefBrowserInit(this.browser)
        }
        API.loadPageCefBrowser(this.browser, url)
        this.position = new Point(x, y)
        this.visible = visible
    }

    get visible(){
        return !API.getCefBrowserHeadless(this.browser)
    }
    set visible(toggle){
        API.setCefBrowserHeadless(this.browser, !toggle)
    }

    get position(){
        return API.getCefBrowserPosition(this.browser)
    }
    set position(point: Point){
        API.setCefBrowserPosition(this.browser, point.X, point.Y)
    }

    get size(){
        return API.getCefBrowserSize(this.browser)
    }
    set size(size){
        API.setCefBrowserSize(this.browser, size.Width, size.Height)
    }

    dispose(){
        API.destroyCefBrowser(this.browser)
    }
}
