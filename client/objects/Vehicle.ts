// Vehicle härstammar och utökar från Entity
class Vehicle extends Entity {
    // Första constructor (ctor) för en ny bil 
    constructor(model, pos, rot);
    // Andra constructor
    // Tillåter inmatning av existerande bil ID
    constructor(handle, model?, pos?, rot?){
        if(model != undefined){
            super(API.createVehicle(
                model,
                pos,
                rot
            ))
        } else super(handle);
    }

    get radioEnabled(){
        return API.getVehicleRadioEnabled()
    }
    set radioEnabled(toggle){
        API.setVehicleRadioEnabled(this.handle, toggle)
    }

    get radioStation(){
        return API.getCurrentVehicleRadioStationId()
    }
    set radioStation(id){
        API.setVehicleRadioStation(this.handle, id)
    }
}