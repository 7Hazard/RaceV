﻿class Player {
    // Accessors

    static get inVehicle(){
        return API.isPlayerInAnyVehicle(this.handle)
    }

    static get vehicle(){
        return new Vehicle(API.getPlayerVehicle(Player.handle))
    }

    static get handle() {
        return API.getLocalPlayer()
    }

    static get position() {
        return API.getEntityPosition(this.handle);
    }
    static set position(pos) {
        API.setEntityPosition(this.handle, pos);
    }

    static get rotation() {
        return API.getEntityRotation(this.handle);
    }
    static set rotation(rot) {
        API.setEntityRotation(this.handle, rot);
    }

    static get frozen() {
        return API.isEntityPositionFrozen(this.handle);
    }
    static set frozen(freeze) {
        API.setEntityPositionFrozen(this.handle, freeze);
    }

    private static isControlsDisabled = false
    static get controlsDisabled(){
        return this.isControlsDisabled
    }
    static set controlsDisabled(toggle: boolean){
        this.isControlsDisabled = toggle
        API.disableAllControls(toggle)
    }

    static get invincible(){
        return API.getPlayerInvincible()
    }
    static set invincible(toggle){
        API.setPlayerInvincible(toggle)
    }

    static get camPosition() {
        return API.getGameplayCamPos();
    }

    static get camRotation() {
        return API.getGameplayCamRot();
    }
    static set camHeading(angle) {
        API.callNative(
            "0xB4EC2312F4E5B1F1", 
            angle
        )
    } 

    // Functions
    static update() {
        //if (this.frozen) {
        //    API.disableAllControlsThisFrame();
        //}
    }
}
