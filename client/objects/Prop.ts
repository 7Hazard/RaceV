// Prop härstammar och utökar från Entity
// I princip endast constructorn som skiljer sig
class Prop extends Entity {
    constructor(
        model: number, 
        pos: Vector3, 
        rot: Vector3
    ){
        super(API.createObject(
            model,
            pos,
            rot
        ));
    }
}