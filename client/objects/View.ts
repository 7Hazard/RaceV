
class View {
    static get resolution(){
        return API.getScreenResolutionMaintainRatio()
    }

    static get width(){
        return this.resolution.Width
    }

    static get height(){
        return this.resolution.Height
    }

    static toWorld(x, y){
        return API.screenToWorldMaintainRatio(new PointF(x, y))
    }
}