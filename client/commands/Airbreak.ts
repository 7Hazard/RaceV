class Airbreak {
    /* Statics */
    private static enabled = false

    static toggle() {
        this.enabled = !this.enabled
        if (this.enabled) {
            Player.frozen = true;
            //API.disableAllControls(true);
            API.sendNotification("Airbreak ON");
        } else {
            Player.frozen = false;
            //API.disableAllControls(false);
            API.sendNotification("Airbreak OFF");
        }
    }

    static update() {
        if (this.enabled) {
            let pos = Player.position
            let rot = API.getGameplayCamRot()

            // pos setter
            let dir = false;
            let speed = 1
            let angle = 0
            let setpos = (
                x = (Math.cos(angle) * speed),
                y = (Math.sin(angle) * speed),
                z = 0
            ) => {
                Player.position = pos = new Vector3(
                    pos.X + x,
                    pos.Y + y,
                    pos.Z + z
                )
            }

            // Adjusters
            if (Control.isPressed(GameControl.SPRINT)) { // Shift
                speed = 10
            }
            if (Control.isPressed(GameControl.CHARACTER_WHEEL)) { // Alt
                speed = 0.1
            }
            if (Control.isPressed(GameControl.JUMP)) { // Space
                setpos(0, 0, speed)
                dir = true
            }
            if (Control.isPressed(GameControl.DUCK)) { // L CTRL
                setpos(0, 0, -speed - 1)
            }

            // Direction
            if (Control.isPressed(GameControl.MOVE_UP_ONLY)) { // W
                angle = rot.Z * (Math.PI / 180) + (Math.PI / 2)
                setpos()
                dir = true
            }
            if (Control.isPressed(GameControl.MOVE_DOWN_ONLY)) { // Backward/S
                angle = (rot.Z * (Math.PI / 180)) - (Math.PI / 2)
                setpos()
                dir = true
            }
            if (Control.isPressed(GameControl.MOVE_LEFT_ONLY)) { // A
                angle = (rot.Z * (Math.PI / 180)) + (Math.PI)
                setpos()
                dir = true
            }
            if (Control.isPressed(GameControl.MOVE_RIGHT_ONLY)) { // D
                angle = rot.Z * (Math.PI / 180)
                setpos()
                dir = true
            }

            if (dir) {
                pos.Z--;
                Player.position = pos;
            }

            API.drawText(
                "Pos: " + pos.X.toFixed(1)
                + ", " + pos.Y.toFixed(1)
                + ", " + pos.Z.toFixed(1)
                + "\nRot: " + rot.X.toFixed(1)
                + ", " + rot.Y.toFixed(1)
                + ", " + rot.Z.toFixed(1),

                View.width / 2 - 100, View.height - 80, 0.5,
                255, 255, 255, 255,
                4, 0, false, false, 0
            )
        }
    }
}