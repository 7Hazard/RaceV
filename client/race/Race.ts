class Race {
    /* Statics */
    static waiting = true;
    static data; // Track json data
    static props:Prop[] = []; // All rekvesita
    static nextCheckpoint: Checkpoint; // Nästa checkpoint
    static afterCheckpoint: Checkpoint; // Checkpointen efter nästa
    static lastCheckpoint = -1; // Sista nådda checkpointen
    static finished = false; // Om racet är färdigt
    static ready = false; // Om spelaren är redo
    static controllerInfo:WebView; // Webbvy för kontroll information

    // Called när spelläget ska städa upp
    static dispose(){
        this.controllerInfo.dispose(); // Ta bort webbvyn
    }

    // Alla events som kommer från servern
    static onEvent(args: System.Array<any>){
        switch(args[0]){
            case "load":
            this.data = JSON.parse(args[1]) // Sätt JSON datan
            this.load() // Ladda racingbanan
            break;

            case "start":
            this.start(args[1]) // Starta racet
            break

            case "checkpoint":
            this.checkpoint(args[1]); // En checkpoint är nådd
            break;

            case "finish":
            this.finish(args[1], args[2], args[3], args[4], args[5]); // En spelare nådde målet
            break;
        }
    }

    // När en spelare har nått målet
    static finish(won: boolean, x, y, z, yaw){
        if(!this.finished){ // Om spelet inte är redan färdigt
            this.finished = true; // Färdigställ spelet
            if(won){ // Om det var den lokala spelaren som nått målet
                API.showColoredShard("Vinnare", "Du kom på första plats", 1, 12, 3000);
                API.startCoroutine(function*(){ // Vänta 3 sekunder och starta om
                    yield 3000;
                    API.triggerServerEvent("reload"); // Skicka event till servern
                });
            } else { // Om spelaren inte var vinnaren (förlorade)
                API.showColoredShard("Förlust", "Du kom andra plats", 1, 7, 3000);
                Player.controlsDisabled = true
                Player.vehicle.velocity = new Vector3(0,0,0); // Stanna bilen
            }
        }

        // Rengör banan
        this.nextCheckpoint.dispose();
        this.afterCheckpoint.dispose();

        // Skapa en kamera bakom vinnaren och skaka
        let lcam = new Camera(
            new Vector3(x, y, z),
            new Vector3(10, 0, yaw+1)
        )
        lcam.setActive();
        lcam.shake("JOLT_SHAKE", 1);
    }

    // När en checkpoint är nådd
    static checkpoint(i){
        if(i == this.lastCheckpoint+1){ // Om det var nästa checkpoint som nåddes
            this.lastCheckpoint++;
            this.nextCheckpoint.dispose(); // Ta bort nästa checkpointen

            if(this.data.checkpoints[i+2] != undefined){ // Om nästa checkpoint inte är sista
                // Förbered data för nästa checkpoint
                let cp = this.data.checkpoints[i+1];
                let dir = this.data.checkpoints[i+2].pos;
                let pos = cp.pos;
                let color = cp.color;

                // Skapa nästa checkpoint
                this.nextCheckpoint = new Checkpoint(
                    MarkerType.None,
                    new Vector3(pos.x, pos.y, pos.z),
                    API.directionToRotation(new Vector3(dir.x, dir.y, dir.z)),
                    new Vector3(cp.radius*2, cp.radius*2, cp.height),
                    new RGBA(color.r, color.g, color.b, color.a)
                );
                
                // Förbered data för nästa checkpoint
                this.afterCheckpoint.dispose();
                cp = this.data.checkpoints[i+2];
                pos = cp.pos;
                color = cp.color;
                
                // Skapa checkpoint efter nästa
                this.afterCheckpoint = new Checkpoint(
                    MarkerType.None,
                    new Vector3(pos.x, pos.y, pos.z),
                    new Vector3(),
                    new Vector3(cp.radius*2, cp.radius*2, cp.height),
                    new RGBA(color.r, color.g, color.b, color.a)
                );
            } else if(this.data.checkpoints[i+1] != undefined) { // Om nästa checkpoint finns (om sista)
                // Förbered data för nästa checkpoint
                this.afterCheckpoint.dispose(); // Ta bort gammal checkpoint
                let cp = this.data.checkpoints[i+1];
                let pos = cp.pos;
                let color = cp.color;

                // Skapa sista checkpoint
                this.nextCheckpoint = new Checkpoint(
                    MarkerType.CheckeredFlagRect,
                    new Vector3(pos.x, pos.y, pos.z),
                    new Vector3(0, 0, 20),
                    new Vector3(cp.radius*2, cp.radius*2, cp.height),
                    new RGBA(color.r, color.g, color.b, color.a)
                );
            } else{ // Om den nådda checkpointen var målet
                API.triggerServerEvent("race", "finish"); // Meddela servern om att spelaren nådda målet
            }
            API.sendNotification("Checkpunkt "+i);
            API.setWaypoint(this.nextCheckpoint.position.X, this.nextCheckpoint.position.Y) // Sätt vägmarkering på mini-map
        }
    }

    // Ladda racingbanan
    static load(){
        // Skapa webb vyn med information för kontrollerna
        this.controllerInfo = new WebView(
            "client/race/info/index.html", true,
            0, 0, View.height/2.5, View.width/4
        )
        API.sendNotification("Loading track "+this.data.name)
        Player.controlsDisabled = true // Stäng av kontrollerna

        // Lägg spelaren hos en spawn
        // Spawn  hämtad från track data
        let spawn = this.data.spawns[0].pos;
        Player.position = new Vector3(spawn.x, spawn.y, spawn.z);

        // Checkpoint placering från track data
        if (this.data.checkpoints[1] != undefined){ // Om den andra checkpointen finns
            // Förbered data för första checkpointen
            let cp = this.data.checkpoints[0];
            let dir = this.data.checkpoints[1].pos;
            let pos = cp.pos;
            let color = cp.color;

            // Skapa checkpoint
            this.nextCheckpoint = new Checkpoint(
                MarkerType.None,
                new Vector3(pos.x, pos.y, pos.z),
                API.directionToRotation(new Vector3(dir.x, dir.y, dir.z)),
                new Vector3(cp.radius*2, cp.radius*2, cp.height),
                new RGBA(color.r, color.g, color.b, color.a)
            );

            // Förbered data för checkpointen efter
            cp = this.data.checkpoints[1];
            pos = cp.pos;
            color = cp.color;

            // Skapa checkpointen
            this.afterCheckpoint = new Checkpoint(
                MarkerType.None,
                new Vector3(pos.x, pos.y, pos.z),
                new Vector3(),
                new Vector3(cp.radius*2, cp.radius*2, cp.height),
                new RGBA(color.r, color.g, color.b, color.a)
            );
        } else {
            // Sätt första checkpointen som målet
            let cp = this.data.checkpoints[0];
            let pos = cp.pos;
            let color = cp.color;
            this.nextCheckpoint = new Checkpoint(
                MarkerType.CheckeredFlagRect, // Mål markering
                new Vector3(pos.x, pos.y, pos.z),
                new Vector3(),
                new Vector3(cp.radius*2, cp.radius*2, cp.height),
                new RGBA(color.r, color.g, color.b, color.a)
            );
        }

        this.data.props.forEach(element => { // För varje rekvesita i datan
            // Förbered datan för respektive rekvesita
            let pos = element.pos;
            let rot = element.rot;

            // Skapa objektet
            this.props.push(new Prop(
                element.model,
                new Vector3(pos.x, pos.y, pos.z),
                new Vector3(rot.x, rot.y, rot.z)
            ));
        });

        // Skapa scenkamera
        let cam = new Camera(new Vector3(740, 1355, 428), new Vector3(0, 0, 180))
        cam.setActive()
    }

    // Startar racet med angiven id
    static start(i){
        this.controllerInfo.dispose(); // Stäng ned informationsfönstret
        API.startCoroutine(function*() {
            yield 100; // Vänta 100 ms

            Race.waiting = false; // Väntar inte längre
            API.sendNotification("Startar racet")
            Player.vehicle.radioEnabled = false; // Stäng av radio

            // Förbered camera animation vid respektive startposition
            let spawn = Race.data.spawns[i];
            Player.rotation = new Vector3(0, 0, spawn.heading)
            Player.camHeading = 0;
            let tcam = new Camera(
                Player.camPosition,
                Player.camRotation
            );
            tcam.setActive(1000);

            API.setWaypoint(Race.nextCheckpoint.position.X, Race.nextCheckpoint.position.Y) // Sätt första vägmarkering
            yield 1500;

            API.setActiveCamera(null) // Byt camera tillbaka till spelaren
            yield 1000;

            // Nedräkning
            API.showColoredShard("3", "", 2, 1, 1000);
            yield 1000;

            API.showColoredShard("2", "", 2, 1, 1000);
            yield 1000;

            API.showColoredShard("1", "", 2, 1, 1000);
            yield 1000;

            API.showColoredShard("KÖR", "", 2, 1, 1000);

            Player.controlsDisabled = false // Sätt på kontrollerna
        });
    }

    // Kallas varje skript updatering
    static onUpdate(){
        if(this.waiting){ // Om spelaren väntar
            if(this.ready){ // Om spelaren är redo
                API.drawText( // Visa text om att spelaren är redo och väntar på andra spelare
                    "Redo\nVäntar på spelare",
                    View.width/1.7-View.width/7, View.height/3-View.height/10, 2, 
                    255, 255, 255, 255,
                    4, 0, false, false, 0
                )
            } else {
                API.drawText( // Visar text om info för att bli redo
                    "Tryck på A om du är redo",
                    View.width/1.7-View.width/7, View.height/3-View.height/10, 2, 
                    255, 255, 255, 255,
                    4, 0, false, false, 0
                )
                if(Control.justPressed(GameControl.SPRINT)){ // Om spelaren tryckte på A (skift på tangentbordet)
                    this.ready = true;
                    API.triggerServerEvent("race", "ready"); // Meddela servern om att spelaren är redo
                }
            }
        } else { // Om spelaren inte väntar
            Control.disable(GameControl.VEH_EXIT); // Stäng av kontrollen för att gå ur bilen
            Control.disable(GameControl.NEXT_CAMERA); // Stäng av kontrollen för att byta kamera zoom
        }
    }
}