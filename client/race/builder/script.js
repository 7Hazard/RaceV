
var data = ["checkpoint"]

$(document).ready(function() {
    
    // Tab switching
    $('#tabs li').on('click', function() {
        var tab = $(this).data('tab');

        $('#tabs li').removeClass('is-active');
        $(this).addClass('is-active');

        $('.tab-content').removeClass('is-active');
        $('#'+tab).addClass('is-active');

        data[0] = tab
        resourceCall("TrackBuilder.updateData", data)
    });

    // Object placement data update

});