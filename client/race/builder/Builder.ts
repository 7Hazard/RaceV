class TrackBuilder{
    /* Static */
    private static cam: Camera
    private static entered = false
    private static menu: WebView
    private static worldPos = new Vector3()
    private static yaw: number = 0
    private static placing: Entity
    private static data: any[] = [
        "checkpoint",
        0, 0, 0,
        -1,
        20, 20, 20,
        255, 204, 0, 100
    ] 

    static onEvent(args: System.Array<any>){
        switch (args[0]){
            
        }
    }

    static updateData(data){
        this.data = data

        if(this.placing != undefined){
            this.placing.dispose()
        }
        switch(this.data[0]){
            case "checkpoint":
            this.placing = new Checkpoint(
                this.data[4],
                this.worldPos,
                new Vector3(0, 0, this.yaw),
                new Vector3(this.data[5], this.data[6], this.data[7]),
                new RGBA(this.data[8], this.data[9], this.data[10], this.data[11])
            )
            break

            case "prop":
            // obj = new Prop
            break
        }
        API.sendNotification("Updated data")
    }

    static init(){
        this.menu = new WebView(
            "client/race/builder/index.html", true,
            10, 10, View.width/5, View.height/3,
            false, false
        )

        this.updateData(this.data)
    }

    static toggle(){
        this.entered = !this.entered
        Entity.disposeAll()
        
        if(this.entered){
            this.updateData(this.data);
            this.cam = new Camera(
                Player.position.Add(new Vector3(0,0,10)),
                Player.rotation
            )
            this.cam.setActive(1000)
            Player.frozen = true
            API.sendNotification("Entered Track Builder")
        } else {
            Player.position = this.cam.position
            this.cam.destroy()
            Player.frozen = false
            this.menu.visible = false
            API.sendNotification("Exited Track Builder")
        }
    }

    static toggleMenu(){
        if(this.entered){
            this.menu.visible = !this.menu.visible
            Cursor.enabled = !Cursor.enabled
        }
    }

    static update(){
        if(this.entered){
            // Set camera rotation movement
            this.cam.rotation = API.getGameplayCamRot();
            
            // Movement
            let pos = this.cam.position
            let speed = 1
            let angle = 0
            let setpos = (
                x=(Math.cos(angle) * speed),
                y=(Math.sin(angle) * speed),
                z = 0
            ) => {
                this.cam.position = pos = new Vector3(
                    pos.X + x,
                    pos.Y + y,
                    pos.Z + z
                )
            }

            if(!this.menu.visible){
                // Adjusters
                if (Control.isPressed(GameControl.SPRINT)) { // Shift
                    speed = 10
                }
                if (Control.isPressed(GameControl.CHARACTER_WHEEL)) { // Alt
                    speed = 0.1
                }
                if (Control.isPressed(GameControl.JUMP)) { // Space
                    setpos(0, 0, speed)
                }
                if (Control.isPressed(GameControl.DUCK)) { // L CTRL
                    setpos(0, 0, -speed)
                }

                // Direction
                if (Control.isPressed(GameControl.MOVE_UP_ONLY)) { // W
                    angle = API.getGameplayCamRot().Z * (Math.PI / 180) + (Math.PI / 2)
                    setpos()
                }
                if (Control.isPressed(GameControl.MOVE_DOWN_ONLY)) { // Backward/S
                    angle = (API.getGameplayCamRot().Z * (Math.PI / 180)) - (Math.PI / 2)
                    setpos()
                }
                if (Control.isPressed(GameControl.MOVE_LEFT_ONLY)) { // A
                    angle = (API.getGameplayCamRot().Z * (Math.PI / 180)) + (Math.PI)
                    setpos()
                }
                if (Control.isPressed(GameControl.MOVE_RIGHT_ONLY)) { // D
                    angle = API.getGameplayCamRot().Z * (Math.PI / 180)
                    setpos()
                }
            }
            
            API.drawGameTexture(
                "darts", "dart_reticules",
                View.width/2-15, View.height/2-15, 30, 30, 0, 
                255, 255, 255, 255
            )

            this.worldPos = View.toWorld(View.width/2, View.height/2)

            // Info
            API.drawText(
                "M: Menu"
                + "\nPlacing "+this.data[0]
                + "\nX: " + this.worldPos.X.toFixed(1)
                + ", Y: " + this.worldPos.Y.toFixed(1)
                + ", Z: " + this.worldPos.Z.toFixed(1)
                + "\nYaw: "+this.yaw,

                View.width/2-100, View.height-140, 0.5, 
                255, 255, 255, 255,
                4, 0, false, false, 0
            );

            // Preview placement
            if(typeof this.placing != undefined){
                this.placing.position = this.worldPos
                //this.placing.rotation = new Vector3(146, 76, this.yaw)
            }
            
            switch(this.data[0]){
                case "checkpoint":
                
                break

                case "prop":
                break
            }

            // Object placement
            if(Control.justPressed(GameControl.ATTACK)){
                API.sendNotification("Placing "+this.data[0]);
                this.data[1] = this.worldPos.X;
                this.data[2] = this.worldPos.X;
                this.data[3] = this.worldPos.X;
                API.triggerServerEvent("TrackBuilder", "place", ...this.data)
            }
            if(Control.isPressed(GameControl.WEAPON_WHEEL_PREV)){
                this.yaw+=2;
            }
            if(Control.isPressed(GameControl.WEAPON_WHEEL_NEXT)){
                this.yaw-=2;
            }
        }
    }
}
