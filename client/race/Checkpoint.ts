class Checkpoint extends Marker {
    /// Members
    markerType: MarkerType;
    marker: Marker;

    constructor(
        type: MarkerType,
        pos: Vector3,
        rot: Vector3,
        scale: Vector3,
        rgba: RGBA
    ){
        super(
            MarkerType.Cylinder,
            pos,
            new Vector3(),
            scale,
            rgba
        )
        this.markerType = type;
        if(type != MarkerType.None){
            this.marker = new Marker(
                type,
                pos.Add(new Vector3(0, 0, 10)),
                rot,
                scale.Divide(2),
                rgba
            )
        }
    }

    dispose(){
        super.dispose()
        if(this.markerType != MarkerType.None){
            this.marker.dispose()
        }
    }

    //get position(){
    //    return super.position
    //}
    //set position(pos: Vector3){
    //    super.position = pos
    //    //this.marker.position = pos.Add(new Vector3(0,0,8))
    //}
//
    //get rotation(){
    //    return this.marker.rotation
    //}
    //set rotation(rot: Vector3){
    //    this.marker.rotation = rot
    //}
//
    //get direction(){
    //    return this.marker.direction
    //}
    //set direction(dir){
    //    this.marker.direction = dir
    //}
}
