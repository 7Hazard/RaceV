class RGBA {
    r: number
    g: number
    b: number
    a: number

    constructor(
        red,
        green,
        blue,
        alpha = 255
    ){
        this.r = red
        this.g = green
        this.b = blue
        this.a = alpha
    }

    get red(){
        return this.r
    }
    get green(){
        return this.g
    }
    get blue(){
        return this.b
    }
    get alpha(){
        return this.a
    }
}