﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using RaceV.server.commands;
using RaceV.server.racing;

namespace RaceV.server
{
    public class RaceV : Script
    {
        public static API api;

        public RaceV()
        {
            api = API;
            API.onResourceStart += OnStart;
            API.onResourceStop += API_onResourceStop;
            API.onPlayerFinishedDownload += OnConnect;
            API.onPlayerDisconnected += API_onPlayerDisconnected;
            API.onClientEventTrigger += API_onClientEventTrigger;
        }

        private void API_onClientEventTrigger(Client sender, string eventName, params object[] arguments)
        {
            switch (eventName)
            {
                //case "TrackBuilder":
                //    TrackBuilder.OnEvent(sender, arguments);
                //    break;
                case "reload":
                    API.restartResource("RaceV");
                    break;
                case "race":
                    Race.OnEvent(sender, arguments);
                    break;
                default:
                    break;
            }
        }

        private void API_onResourceStop()
        {
            Race.Dispose();
        }

        void OnConnect(Client player)
        {
            // Lägg till spelaren i racet
            Race.Add(player);
        }

        private void API_onPlayerDisconnected(Client player, string reason)
        {
            Race.Remove(player);
        }

        void OnStart()
        {
            Race.Init();
        }
    }
}
