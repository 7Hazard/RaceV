﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaceV.server.structs
{
    public class RGBA
    {
        public byte r, g, b, a;

        public RGBA(byte red, byte green, byte blue, byte alpha)
        {
            this.r = red;
            this.g = green;
            this.b = blue;
            this.a = alpha;
        }
    }
}
