﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared.Math;
using RaceV.server.racing;
using RaceV.server.structs;
using System.Collections.Generic;

namespace RaceV.server.commands
{
    class Commands : Script
    {

        #region server
        [Command]
        public void reload(Client player, string resource = null)
        {
            if (resource == null)
            {
                API.restartResource("RaceV");
            } else
            {
                API.restartResource(resource);
            }
        }
        #endregion

        #region entities
        [Command]
        public void veh(Client player, string name, int color1 = 0, int color2 = 0, int dimension = 0)
        {
            var veh = API.createVehicle(API.getHashKey(name), player.position, player.rotation, color1, color2, dimension);
            player.setIntoVehicle(veh, -1);
        }

        [Command]
        public void marker(Client player)
        {
            API.createMarker(1,
                player.position,
                new Vector3(),
                player.rotation,
                new Vector3(10, 10, 15),
                255, 255, 255, 255
                );
        }

        List<Checkpoint> checkpoints = new List<Checkpoint>();
        [Command]
        public void checkpoint(Client player, bool last = false, bool bob = false)
        {
            var c = new Checkpoint(
                player.position,
                player.rotation,
                new Vector3(10, 10, 15),
                new RGBA(255, 204, 0, 100),
                bob
                );
            checkpoints.Add(c);
        }

        [Command]
        public void prop(Client player, string name)
        {
            API.createObject(API.getHashKey(name), player.position, player.rotation);
        }

        [Command]
        public void clean(Client player)
        {
            API.triggerClientEventForAll("clean");
        }
        #endregion

        #region position
        [Command]
        public void air(Client player)
        {
            API.triggerClientEvent(player, "toggleAirbreak");
        }

        [Command]
        public void pos(Client player, int x, int y, int z, string playername = null)
        {
            if (playername != null)
            {
                player = API.getPlayerFromName(playername);
            }

            if (player.isInVehicle)
            {
                player.vehicle.position = new Vector3(x, y, z);
                player.vehicle.rotation = new Vector3(0, 0, 0);
            }
            else
            {
                player.position = new Vector3(x, y, z);
            }
        }
        #endregion

        #region race
        [Command]
        public void start(Client player)
        {
            Race.Start();
        }
        #endregion

        #region building
        [Command]
        public void build(Client player)
        {
            API.triggerClientEvent(player, "toggleBuilder");
        }

        //[Command]
        //public void undo(Client player)
        //{
        //    TrackBuilder.Undo();
        //}
        //
        //[Command]
        //public void save(Client player, string name)
        //{
        //    TrackBuilder.Save(name);
        //}
        #endregion
    }
}
