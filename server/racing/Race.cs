﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared.Math;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaceV.server.racing
{
    public class Race
    {
        static API api = RaceV.api;

        public static List<CylinderColShape> checkpointsCollisions = new List<CylinderColShape>();
        static List<Client> players = new List<Client>();
        static List<Client> ready = new List<Client>();

        public static void OnEvent(Client client, params object[] args)
        {
            switch (args[0])
            {
                case "finish":
                    Finish(client);
                    break;
                case "ready":
                    Ready(client);
                    break;
            }
        }

        static void Ready(Client client)
        {
            ready.Add(client);

            if (players.Count == ready.Count)
                Start();
        }

        public static void Init()
        {
            RaceTrack.Init();

            // Ladda en bana slumpmässigt
            RaceTrack.Load();
        }

        public static void Dispose()
        {
            checkpointsCollisions.Clear();
            players.Clear();
            ready.Clear();
        }

        public static void Start()
        {
            for (int i = 0; i < players.Count; i++)
            {
                var player = players[i];
                var spawn = RaceTrack.current.spawns[i];
                var veh = api.createVehicle(api.getHashKey("Prototipo"), spawn.pos, new Vector3(0, 0, spawn.heading), 0, 0);
                veh.waitForPlayerSynchronization(player);
                player.setIntoVehicle(veh, -1);
                player.seatbelt = true;
                player.triggerEvent("race", "start", i);
            }
        }

        public static void Finish(Client winner)
        {
            winner.triggerEvent("race", "finish", true,
                winner.position.X, winner.position.Y, winner.position.Z, winner.vehicle.rotation.Z);
            foreach (var ply in players)
            {
                if (ply != winner)
                {
                    ply.triggerEvent("race", "finish", false,
                        winner.position.X, winner.position.Y, winner.position.Z, winner.vehicle.rotation.Z);
                }
            }
        }

        public static void Add(Client client)
        {
            client.invincible = true;
            players.Add(client);
            if (RaceTrack.current != null)
            {
                client.triggerEvent("race", "load", RaceTrack.current.json); // Bygg banan hos spelaren
            } else
            {
                client.triggerEvent("race", "load");
            }
        }

        public static void Remove(Client client)
        {
            players.Remove(client);
        }
    }
}
