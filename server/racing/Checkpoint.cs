﻿using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using RaceV.server.structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaceV.server.racing
{
    public class Checkpoint
    {
        Marker cylinder;
        Marker pointer;

        public Checkpoint(Vector3 pos, Vector3 rot, Vector3 scale, RGBA color, bool bob = false)
        {
            cylinder = RaceV.api.createMarker(
                1,
                pos.Add(new Vector3(0, 0, -1)),
                new Vector3(),
                new Vector3(),
                scale,
                color.a, color.r, color.g, color.b
                );
            /*pointer = Main.api.createMarker(
                3,
                pos,
                new Vector3(),
                rot.Add(new Vector3(90, 0, 0)),
                scale.Divide(2),
                color.alpha, color.red, color.green, color.blue,
                bobUpAndDown: bob
                );*/
        }

        ~Checkpoint()
        {
            
        }
    }
}
