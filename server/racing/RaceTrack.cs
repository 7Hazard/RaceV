﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;
using Newtonsoft.Json;
using RaceV.server.structs;
using System;
using System.Collections.Generic;
using System.IO;

namespace RaceV.server.racing
{
    public class RaceTrack
    {
        static API api = RaceV.api;

        static Random random = new Random();
        static List<string> available = new List<string>();
        public static RaceTrack current;

        public static void Init()
        {
            Directory.CreateDirectory("tracks");
            foreach (var path in Directory.EnumerateFiles("tracks", "*.json"))
            {
                var v1 = path.LastIndexOf('\\')+1;
                var v2 = path.LastIndexOf('.')-v1;
                string name = path.Substring(v1, v2);
                api.consoleOutput("Loading race track " + name);
                available.Add(name);
            }
        }

        public static void Load(string name)
        {
            var json = File.ReadAllText("tracks/" + name + ".json");
            current = JsonConvert.DeserializeObject<RaceTrack>(json);
            current.json = json;

            for (int i = 0; i < current.checkpoints.Count; i++)
            {
                var cp = current.checkpoints[i];
                int curcp = i;
                var col = api.createCylinderColShape(cp.pos, cp.radius, cp.height);
                col.onEntityEnterColShape += (ColShape shape, NetHandle entity) =>
                {
                    var ply = api.getPlayerFromHandle(entity);
                    if (ply != null)
                    {
                        ply.triggerEvent("race", "checkpoint", curcp);
                    }
                };
                Race.checkpointsCollisions.Add(col);
            }
        }

        public static void Load()
        {
            var rand = Random();
            if (rand != null)
            {
                Load(rand);
            }
        }

        public static string Random()
        {
            if (available.Count > 0)
            {
                return available[random.Next(available.Count - 1)];
            } else
            {
                return null;
            }
        }

        // Members
        [JsonIgnore]
        public string json;

        public string name;
        public List<SpawnData> spawns { get; set; }
        public List<CheckpointData> checkpoints { get; set; }

        // Structures
        public struct SpawnData
        {
            public Vector3 pos { get; set; }
            public float heading { get; set; }
        }

        public struct CheckpointData
        {
            public Vector3 pos { get; set; }
            public float radius;
            public float height;
            public RGBA color { get; set; }
        }
    }
}
